package mmstutorhelper.model;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

/**
 * Created by Patrick on 10.05.2018.
 */
public class Submission {
    private ZipFile zipFile;
    private List<ZipEntry> entrys;
    private String submissionID;
    private String folder;
    private  Student student;
    public Submission(String folder, List<ZipEntry> entrys, ZipFile zipFile){
        this.zipFile=zipFile;
        this.entrys=entrys;
        this.folder=folder;
        submissionID=entrys.get(0).getName().split("_")[0];
    }
    public String getSubmissionID() {
        return submissionID;
    }
    public void extractTo(Path folder) throws IOException{
        clearDirectory(folder.toFile());
        entrys.forEach((e) -> {
            try {
                extractZip(zipFile.getInputStream(e),folder,e,null);
            }catch(IOException e1){ErrorHandler.getInstanceOf().addError(e1,"Error extracting Submission-zip!");}
        });
    }
    private void extractZip(InputStream inputStream,Path folder,ZipEntry entry,String repEx) throws IOException {
        String fileToExtraceTo=entry.getName().replace(this.folder+"/","").replace("/",File.separator);
        if(repEx!=null)
            fileToExtraceTo=fileToExtraceTo.replace(repEx+File.separator,"");
        if(entry.getName().toLowerCase().endsWith(".zip")) {
            ZipInputStream zipInStr = new ZipInputStream(inputStream);
            ZipEntry subEntry;
            while ((subEntry = zipInStr.getNextEntry()) != null)
                extractZip(zipInStr, Paths.get(folder.toString() + File.separator + entry.getName().replace(this.folder + "/", "").replace(".zip", "")), subEntry, entry.getName().replace(".zip", ""));
        }
        else if(!entry.isDirectory())
            extractFile(inputStream, folder, fileToExtraceTo);
    }
    private void extractFile(InputStream inStr,Path folder,String file) throws IOException {
        Paths.get(folder.toString()+File.separator+file).toFile().getParentFile().mkdirs();
        Files.write(Paths.get(folder.toString()+File.separator+file),inStr.readAllBytes());
    }
    private void clearDirectory(File dir){
        //noinspection ConstantConditions
        for (File file: dir.listFiles()) {
            if (file.isDirectory())
                clearDirectory(file);
            //noinspection ResultOfMethodCallIgnored
            file.delete();
        }
    }
    public void setStudent(Student student) {
        this.student=student;
    }
    public String getMatrNr() {
        return student==null?"":student.getMatrNr();
    }
    public String getSKZ() {
        return student==null?"":student.getSKZ();
    }
    public String getFirstName() {
        return student==null?"":student.getFirstName();
    }
    public String getLastName() {
        return student==null?"":student.getLastName();
    }
    public String getEmail() {
        return student==null?"":student.getEmail();
    }
    public String getUserID(){return student==null?"":student.getUserID();}
    @Override
    public String toString(){
        return submissionID;
    }
}
