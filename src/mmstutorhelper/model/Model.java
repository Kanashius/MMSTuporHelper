package mmstutorhelper.model;

import javafx.application.Platform;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.scene.web.WebEngine;

import javax.xml.transform.TransformerException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by Patrick on 10.05.2018.
 */
public class Model {
    private ObservableMap<String,Course> courses;
    private ObservableMap<String,ExerciseSubmission> exerciseSubmissions;
    private ObservableList<ExerciseSubmission> exerciseSubmissionList;
    private ObservableList<Course> courseList;
    private SimpleStringProperty participantFolder,submissionFolder,ideFolder;
    private WebEngine webEngine;
    private ExtendedWebEngine extendedWebEngine;
    private SimpleStringProperty moodleUsername,moodlePassword;
    private Course selectedCourse;
    private ExerciseSubmission exerciseSubmission;
    private ObservableList<Submission> submissions;
    private SimpleIntegerProperty parts;
    private SimpleIntegerProperty selPart;
    private Map<String,String> userIDs;
    public Model(){
        participantFolder=new SimpleStringProperty();
        submissionFolder=new SimpleStringProperty();
        ideFolder=new SimpleStringProperty();
        moodleUsername=new SimpleStringProperty("");
        moodlePassword=new SimpleStringProperty("");
        courses=FXCollections.observableHashMap();
        courseList=FXCollections.observableArrayList();
        exerciseSubmissions=FXCollections.observableHashMap();
        exerciseSubmissionList=FXCollections.observableArrayList();
        submissions=FXCollections.observableArrayList();
        parts=new SimpleIntegerProperty(1);
        selPart=new SimpleIntegerProperty(0);
        exerciseSubmissions.addListener((MapChangeListener<String, ExerciseSubmission>) change -> {
            if(change.wasAdded()) {
                if(change.getValueRemoved()!=null)
                    Platform.runLater(()->exerciseSubmissionList.remove(change.getValueRemoved())); //temove at viewThread
                Platform.runLater(()->exerciseSubmissionList.add(change.getValueAdded())); //add at viewThread
            }else
                Platform.runLater(()->exerciseSubmissionList.remove(change.getValueRemoved())); //remove at viewThread
        });
        courses.addListener((MapChangeListener<String, Course>) change -> {
            if(change.wasAdded()) {
                if(change.getValueRemoved()!=null)
                    Platform.runLater(()->courseList.remove(change.getValueRemoved())); //temove at viewThread
                Platform.runLater(()->courseList.add(change.getValueAdded())); //add at viewThread
            }else
                Platform.runLater(()->courseList.remove(change.getValueRemoved())); //remove at viewThread
        });
        participantFolder.addListener((observable, oldValue, newValue) -> {
            try {
                courses.clear();
                Files.walk(Paths.get(newValue),1).forEach((path->{
                    if(path.toString().toLowerCase().endsWith(".csv"))
                        try {
                            Course course = new Course(path);
                            courses.put(course.getCourseID(),course);
                        }catch(IOException e){ErrorHandler.getInstanceOf().addError(e,"Error loading Participantlist (CSV-File).");}
                }));
            }catch(IOException e){ErrorHandler.getInstanceOf().addError(e,"Error at Filewalk participants: "+newValue);}
        });
        submissionFolder.addListener((observable, oldValue, newValue) -> {
            try {
                exerciseSubmissions.clear();
                Files.walk(Paths.get(newValue),1).forEach((path->{
                    if(path.toString().toLowerCase().endsWith(".zip"))
                        try{
                            ExerciseSubmission exerciseSubmission=new ExerciseSubmission(path);
                            exerciseSubmissions.put(exerciseSubmission.getExerciseID(),exerciseSubmission);
                        }catch(IOException e){ErrorHandler.getInstanceOf().addError(e,"Error loading Exercise Submissions (ZIP-File).");}
                }));
            }catch(IOException e){ErrorHandler.getInstanceOf().addError(e,"Error at Filewalk submissions: "+newValue);}
        });
        parts.addListener((observable, oldValue, newValue) -> new Thread(this::loadSubmissions).start());
        selPart.addListener((observable, oldValue, newValue) -> new Thread(this::loadSubmissions).start());
    }
    public void init(){
        participantFolder.set(System.getProperty("user.dir")+File.separator+"data"+File.separator+"participants");
        submissionFolder.set(System.getProperty("user.dir")+File.separator+"data"+File.separator+"submissions");
        ideFolder.set(System.getProperty("user.dir")+File.separator+"data"+File.separator+"ideFolder"+File.separator+"src");
    }
    public void loadCourse(Course course){
        selectedCourse=course;
        loadSubmissions();
    }
    public void loadExerciseSubmissions(ExerciseSubmission exsub){
        if(exerciseSubmission!=null)
            try {
                exerciseSubmission.close();
            } catch (IOException e) {ErrorHandler.getInstanceOf().addError(e,"Error closing Exercise Zip-File!");}
        exerciseSubmission=exsub;
        try {
            exerciseSubmission.open();
        } catch (IOException e) {
            ErrorHandler.getInstanceOf().addError(e,"Error opening Exercise Zip-File!");
        }
        loadSubmissions();
    }
    public void loadSubmissions(){
        submissions.clear();
        if(selectedCourse==null||exerciseSubmission==null)
            return;
        List<String> studIds;
        if(parts.get()<=1)
            studIds=selectedCourse.getStudents().stream().map(Student::getID).collect(Collectors.toList());
        else
            studIds=selectedCourse.getStudentsPart(selPart.get(),parts.get()).stream().map(Student::getID).collect(Collectors.toList());
        List<Submission> subs=exerciseSubmission.getSubmissions().values().stream().filter(submission -> studIds.contains(submission.getSubmissionID())).collect(Collectors.toList());
        if(userIDs==null)
            userIDs=getUserIDs();
        List<Student> students=selectedCourse.getStudents();
        subs.forEach(submission -> {
            Optional<Student> data=students.stream().filter(student -> student.getID().equals(submission.getSubmissionID())).findFirst();
            data.ifPresent(student -> {
                submission.setStudent(student);
                student.setUserID(userIDs.get(student.getID()));
            });
        });
        submissions.addAll(subs);
    }
    public void loadSubmissionData(Submission sub){
        if(sub!=null)
            try {
                sub.extractTo(Paths.get(getIdeFolder()));
            } catch (IOException e) {
                ErrorHandler.getInstanceOf().addError(e,"Error extracting submission data!");
            }
    }
    public void loadSubmissionRating(Submission sub){
        if(sub!=null)
            loadWithLogin("https://moodle.jku.at/jku2015/mod/assign/view.php?id=" + exerciseSubmission.getExerciseID() + "&rownum=0&action=grader&userid=" + sub.getUserID());
    }
    public void login(){
        extendedWebEngine.postWait("https://moodle.jku.at/jku2015/" + "login/index.php", new String[][]{{"username", moodleUsername.get()}, {"password", moodlePassword.get()}});
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public Map<String,String> getUserIDs(){
        Map<String,String> idMap=new HashMap<>();
        loadWithLogin("https://moodle.jku.at/jku2015/user/index.php?contextid=900732&id=3709&perpage=5000");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            String table=stringBetween(extendedWebEngine.getHTML(),"<TABLE","</TABLE>").get(0);
            Matcher m=Pattern.compile("(?s)<INPUT(?:.*?)type=\"checkbox\"(?:.*?)class=\"usercheckbox\"(?:.*?)name=\"user(.*?)\"(?:.*?)/>(?:.*?)<TD(?:.*?)class=\"cell c1\"(?:.*?)/>(?:\\s*)(.*?)(?:\\s*)</A>(?:.*?)</TD>").matcher(table);
            while(m.find()){
                idMap.put(m.group(2),m.group(1));
            }
        } catch (TransformerException ignored) {}
        return idMap;
    }
    public void loadWithLogin(String sUrl){
        extendedWebEngine.loadWait(sUrl);
        if(extendedWebEngine.getWebEngine().getLocation().contains("login")) {
            login();
            extendedWebEngine.loadWait(sUrl);
        }
    }
    public void setWebEngine(WebEngine engine) {
        this.webEngine = engine;
        extendedWebEngine=new ExtendedWebEngine(engine);
    }
    public WebEngine getWebEngine() {
        return webEngine;
    }

    private ArrayList<String> stringBetween(String sData, String sStart, String sEnd) {
        ArrayList<String> stringsBetween=new ArrayList<>();
        int iStart=0;
        int iEnd;
        while(true){
            iStart=sData.indexOf(sStart,iStart);
            if(iStart<0)
                break;
            iEnd=sData.indexOf(sEnd,iStart+sStart.length());
            if(iEnd<0) {
                break;
            }
            stringsBetween.add(sData.substring(iStart+sStart.length(),iEnd));
            iStart=iEnd+sEnd.length();
        }
        return stringsBetween;
    }
    public ObservableList<ExerciseSubmission> getExerciseSubmissionList() {
        return exerciseSubmissionList;
    }
    public void setExerciseSubmissionList(ObservableList<ExerciseSubmission> exerciseSubmissionList) {
        this.exerciseSubmissionList = exerciseSubmissionList;
    }
    public ObservableList<Course> getCourseList() {
        return courseList;
    }
    public void setCourseList(ObservableList<Course> courseList) {
        this.courseList = courseList;
    }
    public String getParticipantFolder() {
        return participantFolder.get();
    }
    public SimpleStringProperty participantFolderProperty() {
        return participantFolder;
    }
    public void setParticipantFolder(String participantFolder) {
        this.participantFolder.set(participantFolder);
    }
    public String getSubmissionFolder() {
        return submissionFolder.get();
    }
    public SimpleStringProperty submissionFolderProperty() {
        return submissionFolder;
    }
    public void setSubmissionFolder(String submissionFolder) {
        this.submissionFolder.set(submissionFolder);
    }
    public String getIdeFolder() {
        return ideFolder.get();
    }
    public SimpleStringProperty ideFolderProperty() {
        return ideFolder;
    }
    public void setIdeFolder(String ideFolder) {
        this.ideFolder.set(ideFolder);
    }
    public String getMoodleUsername() {
        return moodleUsername.get();
    }
    public SimpleStringProperty moodleUsernameProperty() {
        return moodleUsername;
    }
    public void setMoodleUsername(String moodleUsername) {
        this.moodleUsername.set(moodleUsername);
    }
    public String getMoodlePassword() {
        return moodlePassword.get();
    }
    public SimpleStringProperty moodlePasswordProperty() {
        return moodlePassword;
    }
    public void setMoodlePassword(String moodlePassword) {
        this.moodlePassword.set(moodlePassword);
    }
    public Course getSelectedCourse() {
        return selectedCourse;
    }
    public void setSelectedCourse(Course selectedCourse) {
        this.selectedCourse = selectedCourse;
    }
    public ExerciseSubmission getExerciseSubmission() {
        return exerciseSubmission;
    }
    public void setExerciseSubmission(ExerciseSubmission exerciseSubmission) {
        this.exerciseSubmission = exerciseSubmission;
    }
    public ObservableList<Submission> getSubmissions() {
        return submissions;
    }
    public void setSubmissions(ObservableList<Submission> submissions) {
        this.submissions = submissions;
    }
    public int getParts() {
        return parts.get();
    }
    public SimpleIntegerProperty partsProperty() {
        return parts;
    }
    public void setParts(int parts) {
        this.parts.set(parts);
    }
    public int getSelPart() {
        return selPart.get();
    }
    public SimpleIntegerProperty selPartProperty() {
        return selPart;
    }
    public void setSelPart(int selPart) {
        this.selPart.set(selPart);
    }
}
