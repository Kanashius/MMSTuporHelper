package mmstutorhelper.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.HashMap;

/**
 * Created by Patrick on 10.05.2018.
 */
public class ErrorHandler {
    private static ErrorHandler errorHandler=new ErrorHandler();
    private ObservableList<Exception> exceptions;
    private HashMap<Exception,String> errorMSGs;
    private ErrorHandler(){
        exceptions=FXCollections.observableArrayList();
        errorMSGs=new HashMap<>();
    }
    public static ErrorHandler getInstanceOf(){
        return errorHandler;
    }
    public void addError(Exception e,String errorMSG){
        exceptions.add(e);
    }
    public ObservableList<Exception> getExceptions(){
        return exceptions;
    }
    public void removeException(Exception e){
        exceptions.remove(e);
    }
    public String getErrorMSG(Exception e){
        return errorMSGs.get(e);
    }
}
