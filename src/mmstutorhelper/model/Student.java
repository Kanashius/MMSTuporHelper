package mmstutorhelper.model;

/**
 * Created by Patrick on 10.05.2018.
 */
public class Student {
    private String matrNr,SKZ,firstName,lastName,email,userID;
    public Student(String matrNr, String SKZ, String firstName, String lastName, String email) {
        this.matrNr = matrNr;
        this.SKZ = SKZ;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }
    public String getMatrNr() {
        return matrNr;
    }
    public String getSKZ() {
        return SKZ;
    }
    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public String getEmail() {
        return email;
    }
    public String getID(){return getFirstName()+" "+getLastName();}
    public void setUserID(String s){userID=s;}
    public String getUserID(){return userID;}
}
