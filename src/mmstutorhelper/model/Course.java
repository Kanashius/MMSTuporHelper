package mmstutorhelper.model;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Patrick on 10.05.2018.
 */
public class Course{
    private String courseID;
    private ArrayList<Student> students;

    public Course(Path listFile) throws IOException {
        students=parseFromCSV(listFile);
        courseID=listFile.getFileName().toString().substring(0, listFile.getFileName().toString().lastIndexOf("."));
    }

    public String getCourseID() {
        return courseID;
    }

    public List<Student> getStudents(){
        return new ArrayList<>(students);
    }

    public List<Student> getStudentsRange(int from,int to){
        return new ArrayList<>(students.subList(from, to));
    }

    public List<Student> getStudentsPart(int part){
        return getStudentsPart(part,4);
    }

    public List<Student> getStudentsPart(int part,int partCount){
        int partSize=students.size()/partCount;
        int left=students.size()%partCount;
        if(part!=partCount-1)
            return getStudentsRange((Math.min(part,left)*(partSize+1))+((part-Math.min(part,left))*partSize),(Math.min(part,left)*(partSize+1))+((part-Math.min(part,left))*partSize)+(left>0?partSize+1:partSize));
        else
            return getStudentsRange((Math.min(part,left)*(partSize+1))+((part-Math.min(part,left))*partSize),students.size());
    }

    private static ArrayList<Student> parseFromCSV(Path listFile) throws IOException{
        return parseFromCSV(listFile,";");
    }
    private static ArrayList<Student> parseFromCSV(Path listFile,String seperator) throws IOException {
        ArrayList<Student> students=new ArrayList<>();
        Files.readAllLines(listFile, StandardCharsets.ISO_8859_1).stream().skip(1).forEach(s -> {
            String[] data = s.split(seperator);
            if (data.length == 5)
                students.add(new Student(data[0], data[1], data[2], data[3], data[4]));
        });
        return students;
    }
    @Override
    public String toString(){
        return courseID;
    }
}
