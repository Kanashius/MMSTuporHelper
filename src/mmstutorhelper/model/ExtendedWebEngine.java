package mmstutorhelper.model;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.concurrent.Worker;
import javafx.scene.web.WebEngine;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;

/**
 * Created by Patrick on 10.05.2018.
 */
public class ExtendedWebEngine {
    private WebEngine engine;
    private ChangeListener<Worker.State> workerListener;
    private boolean websiteLoaded=true;
    public ExtendedWebEngine(WebEngine engine){
        this.engine=engine;
        workerListener= (observable, oldValue, newValue) -> {
            if(newValue== Worker.State.SUCCEEDED||newValue==Worker.State.FAILED) {
                websiteLoaded = true;
                Platform.runLater(() -> engine.getLoadWorker().stateProperty().removeListener(workerListener));
            }
        };
    }
    private void initWait(){
        websiteLoaded=false;
        Platform.runLater(()->engine.getLoadWorker().stateProperty().addListener(workerListener));
    }
    public void post(String url,String[][] params){
        StringBuilder builder=new StringBuilder();
        builder.append("<html><head></head><body>");
        builder.append("<form id=\"post\" method=\"post\" action=\"").append(url).append("\">");
        for(int i=0;i<params.length;i++){
            builder.append("<input id=\""+i+"\" name=\""+params[i][0]+"\" value=\""+params[i][1]+"\" type=\"hidden\">");
        }
        builder.append("</form>");
        builder.append("</body></html>");
        ChangeListener<Document>[] tmp=new ChangeListener[1];
        tmp[0]= (observable, oldValue, newValue) -> {
            Platform.runLater(()->{
                try{engine.executeScript("document.getElementById(\"post\").submit();");
                }catch (Exception e){
                    post(url,params);
                }
            });
            initWait();
            engine.documentProperty().removeListener(tmp[0]);
        };
        engine.documentProperty().addListener(tmp[0]);
        initWait();
        Platform.runLater(()->engine.loadContent(builder.toString()));
    }
    public void postWait(String url,String[][] params){
        post(url,params);
        waitReady();
    }
    public void load(String url){
        initWait();
        Platform.runLater(()->engine.load(url));
    }
    public void loadWait(String url){
        load(url);
        waitReady();
    }
    public void waitReady(){
        while(!websiteLoaded){
            try {
                Thread.sleep(100);
            } catch (InterruptedException ignored) {}
        }
    }
    public WebEngine getWebEngine(){
        return engine;
    }
    public String getHTML() throws TransformerException {
        return getHTML(engine.getDocument());
    }
    public String getHTML(Node node) throws TransformerException {
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
        StringWriter writer = new StringWriter();
        transformer.transform(new DOMSource(node), new StreamResult(writer));
        return writer.toString();
    }
}
