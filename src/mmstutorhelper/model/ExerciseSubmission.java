package mmstutorhelper.model;

import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Created by Patrick on 10.05.2018.
 */
public class ExerciseSubmission {
    private String exerciseID;
    private String exerciseName;
    private ZipFile zipFile;
    private Path path;
    private HashMap<String,Submission> submissions;
    private boolean opened;
    public ExerciseSubmission(Path listFile) throws IOException {
        exerciseName=listFile.getFileName().toString().substring(0, listFile.getFileName().toString().lastIndexOf(".")).split("-")[1];
        exerciseID=listFile.getFileName().toString().substring(0, listFile.getFileName().toString().lastIndexOf(".")).split("-")[2];
        submissions=new HashMap<>();
        path=listFile;
    }
    public void open() throws IOException {
        if(opened)
            close();
        zipFile=new ZipFile(path.toFile());
        Map<String,List<ZipEntry>> subs=zipFile.stream().collect(Collectors.groupingBy(w->w.getName().split("/")[0]));
        subs.forEach((s, zipEntries) -> {
            Submission sub=new Submission(s,zipEntries,zipFile);
            submissions.put(sub.getSubmissionID(),sub);
        });
        opened=true;
    }
    public HashMap<String,Submission> getSubmissions(){
        if(!opened)
            throw new IllegalStateException("You have to open the ExerciseSubmission before requesting the Submissions!");
        return submissions;
    }
    public void close() throws IOException {
        zipFile.close();
        opened=false;
    }
    public String getExerciseID() {
        return exerciseID;
    }
    public String getExerciseName(){return exerciseName;}
    @Override
    public String toString(){
        return exerciseName;
    }
}
