package mmstutorhelper.view;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import mmstutorhelper.model.Course;
import mmstutorhelper.model.ErrorHandler;
import mmstutorhelper.model.ExerciseSubmission;
import mmstutorhelper.model.Model;

import java.io.File;
import java.util.Arrays;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Created by Patrick on 10.05.2018.
 */
public class MainView extends Application {
    private static final Object isReadyNotify=new Object();
    public static MainView lastLoadedMainView;
    private WebView webView;
    private VBox root;
    private Model model;
    private ComboBox<Course> courseSelection;
    private ComboBox<ExerciseSubmission> exerciseSelection;
    private SubmissionList submissionTableView;
    private NumberTextField parts;
    private ComboBox<Integer> partNumber;
    private TextField username;
    private PasswordField password;
    private Button extractFolder;
    private Tooltip extrFolderTooltip;
    private Stage primaryStage;
    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage=primaryStage;
        int width=1000,height=800;
        lastLoadedMainView=this;
        webView=new WebView();
        courseSelection=new ComboBox<>();
        exerciseSelection=new ComboBox<>();
        submissionTableView=new SubmissionList();//MatrNr,Vorname,Nachname,E-Mail,SKZ
        username=new TextField();
        password=new PasswordField();
        extractFolder=new Button();
        extrFolderTooltip=new Tooltip();
        extractFolder.setTooltip(extrFolderTooltip);
        courseSelection.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> new Thread(()->model.loadCourse(newValue)).start());
        exerciseSelection.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> new Thread(()->model.loadExerciseSubmissions(newValue)).start());
        submissionTableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> new Thread(()->model.loadSubmissionRating(newValue)).start());
        submissionTableView.setPrefWidth(500);
        ObservableList<Integer> numberList= FXCollections.observableArrayList();
        parts=new NumberTextField();
        parts.setPromptText("Divide into X parts!");
        partNumber=new ComboBox<>(numberList);
        root=new VBox();
        Label tmp=new Label("Course selection");
        tmp.setTooltip(new Tooltip("Course selection"));
        VBox one=new VBox(tmp,courseSelection);
        one.setSpacing(5);
        tmp=new Label("Exercise selection");
        tmp.setTooltip(new Tooltip("Exercise selection"));
        VBox two=new VBox(tmp,exerciseSelection);
        two.setSpacing(5);
        tmp=new Label("Partitions");
        tmp.setTooltip(new Tooltip("Aufteilung"));
        VBox three=new VBox(tmp,parts);
        three.setSpacing(5);
        tmp=new Label("Partselection");
        tmp.setTooltip(new Tooltip("Teilauswahl"));
        VBox four=new VBox(tmp,partNumber);
        four.setSpacing(5);
        tmp=new Label("Moodleuser");
        tmp.setTooltip(new Tooltip("Moodleuser"));
        VBox five=new VBox(tmp,username);
        five.setSpacing(5);
        tmp=new Label("Moodlepassword");
        tmp.setTooltip(new Tooltip("Moodlepassword"));
        VBox six=new VBox(tmp,password);
        six.setSpacing(5);
        tmp=new Label("Extractfolder");
        tmp.setTooltip(new Tooltip("Extractfolder"));
        VBox seven=new VBox(tmp,extractFolder);
        seven.setSpacing(5);
        HBox top=new HBox();
        top.setSpacing(5);
        top.getChildren().addAll(one,two,three,four,five,six,seven);
        HBox mid=new HBox();
        mid.setSpacing(5);
        mid.getChildren().addAll(submissionTableView,webView);
        root.getChildren().addAll(top,mid);
        root.setSpacing(5);
        primaryStage.setTitle("MMS-Tutor-Helper");
        //Set Size
        mid.prefWidthProperty().bind(root.widthProperty());
        mid.prefHeightProperty().bind(root.heightProperty().subtract(40));
        webView.prefWidthProperty().bind(root.widthProperty().subtract(500));

        primaryStage.setScene(new Scene(root, width, height));
        ErrorHandler.getInstanceOf().getExceptions().addListener((ListChangeListener<Exception>) c -> {
            while(c.next()){
                c.getAddedSubList().forEach((Consumer<Exception>) e -> {
                    Platform.runLater(()->{
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Error Dialog");
                        alert.setHeaderText(e.getMessage());
                        alert.setContentText(Arrays.stream(e.getStackTrace()).map(StackTraceElement::toString).collect(Collectors.joining("\n")));
                        alert.show();
                    });
                });
            }
        });
        primaryStage.show();
        synchronized (isReadyNotify) {
            isReadyNotify.notifyAll();
        }
    }

    public void init(Model model){
        Platform.runLater(()->{
            this.model=model;
            courseSelection.setItems(model.getCourseList());
            exerciseSelection.setItems(model.getExerciseSubmissionList());
            submissionTableView.init(model);
            submissionTableView.setItems(model.getSubmissions());
            model.setWebEngine(webView.getEngine());
            username.textProperty().bindBidirectional(model.moodleUsernameProperty());
            password.textProperty().bindBidirectional(model.moodlePasswordProperty());
            extractFolder.textProperty().bindBidirectional(model.ideFolderProperty());
            extrFolderTooltip.textProperty().bindBidirectional(model.ideFolderProperty());
            parts.textProperty().bindBidirectional(model.partsProperty(), new StringConverter<>() {
                @Override
                public String toString(Number object) {
                    return object.toString();
                }
                @Override
                public Number fromString(String string) {
                    return Integer.parseInt(string);
                }
            });
            model.partsProperty().addListener((observable, oldValue, newValue) -> {
                if(newValue.intValue()!=oldValue.intValue()){
                    partNumber.getItems().clear();
                    for(int i=0;i<newValue.intValue();i++)
                    partNumber.getItems().add(i);
                }
            });
            partNumber.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> model.setSelPart(newValue.intValue()));
            model.selPartProperty().addListener((observable, oldValue, newValue) -> {
                if(!partNumber.getSelectionModel().getSelectedItem().toString().equals(newValue.toString()))
                    partNumber.getSelectionModel().select(newValue.intValue());
            });
            extractFolder.setOnAction(event -> {
                DirectoryChooser chooser = new DirectoryChooser();
                chooser.setTitle("Choose Folder to extract Submissiondata");
                File defaultDirectory = new File(model.getIdeFolder());
                chooser.setInitialDirectory(defaultDirectory);
                File selectedDirectory = chooser.showDialog(primaryStage);
                if(selectedDirectory!=null)
                    model.setIdeFolder(selectedDirectory.getAbsolutePath());
            });
        });
    }

    public static void waitForMainView(){
        //wait for initalization of mmstutorhelper.view
        synchronized (isReadyNotify){
            try {
                MainView.isReadyNotify.wait();
            } catch (InterruptedException ignored) {}
        }
    }
}
