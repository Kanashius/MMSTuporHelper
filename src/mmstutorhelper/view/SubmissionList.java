package mmstutorhelper.view;

import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.*;
import javafx.util.Callback;
import mmstutorhelper.model.Model;
import mmstutorhelper.model.Submission;

/**
 * Created by Patrick on 10.05.2018.
 */
public class SubmissionList extends TableView<Submission>{
    private Model model;
    public void init(Model model){
        this.model=model;
        TableColumn<Submission,Submission> colMatrNr=new TableColumn<>("Matrikel Nummer");
        colMatrNr.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue()));
        colMatrNr.setCellFactory(new Callback<>() {
            @Override
            public TableCell<Submission, Submission> call(TableColumn<Submission, Submission> param) {
                Label lbl=new Label();
                return new TableCell<>() {
                    @Override
                    public void updateItem(Submission item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item != null) {
                            lbl.setText(item.getMatrNr());
                            setGraphic(lbl);
                        }
                        else
                            setGraphic(null);
                    }
                };
            }
        });
        TableColumn<Submission,Submission> colFirstName=new TableColumn<>("First Name");
        colFirstName.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue()));
        colFirstName.setCellFactory(new Callback<>() {
            @Override
            public TableCell<Submission, Submission> call(TableColumn<Submission, Submission> param) {
                Label lbl=new Label();
                return new TableCell<>() {
                    @Override
                    public void updateItem(Submission item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item != null) {
                            lbl.setText(item.getFirstName());
                            setGraphic(lbl);
                        }
                        else
                            setGraphic(null);
                    }
                };
            }
        });
        TableColumn<Submission,Submission> colLastName=new TableColumn<>("Last Name");
        colLastName.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue()));
        colLastName.setCellFactory(new Callback<>() {
            @Override
            public TableCell<Submission, Submission> call(TableColumn<Submission, Submission> param) {
                Label lbl=new Label();
                return new TableCell<>() {
                    @Override
                    public void updateItem(Submission item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item != null) {
                            lbl.setText(item.getLastName());
                            setGraphic(lbl);
                        }
                        else
                            setGraphic(null);
                    }
                };
            }
        });
        TableColumn<Submission,Submission> colEmail=new TableColumn<>("E-Mail");
        colEmail.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue()));
        colEmail.setCellFactory(new Callback<>() {
            @Override
            public TableCell<Submission, Submission> call(TableColumn<Submission, Submission> param) {
                return new TableCell<>() {
                    Label lbl=new Label();
                    @Override
                    public void updateItem(Submission item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item != null) {
                            lbl.setText(item.getEmail());
                            setGraphic(lbl);
                        }
                        else
                            setGraphic(null);
                    }
                };
            }
        });
        TableColumn<Submission,Submission> colLoad=new TableColumn<>("Data");
        colLoad.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue()));
        colLoad.setCellFactory(new Callback<>() {
            @Override
            public TableCell<Submission, Submission> call(TableColumn<Submission, Submission> param) {
                Button btn=new Button("LOAD");
                return new TableCell<>() {
                    @Override
                    public void updateItem(Submission item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item != null) {
                            setGraphic(btn);
                            btn.setOnAction(event -> model.loadSubmissionData(item));
                        }
                        else
                            setGraphic(null);
                    }
                };
            }
        });
        colMatrNr.setPrefWidth(90);
        colFirstName.setPrefWidth(100);
        colLastName.setPrefWidth(100);
        colEmail.setPrefWidth(140);
        colLoad.setPrefWidth(60);
        this.getColumns().addAll(colMatrNr,colFirstName,colLastName,colEmail,colLoad);
    }
}
