package mmstutorhelper;

import javafx.collections.ListChangeListener;
import mmstutorhelper.model.ErrorHandler;
import mmstutorhelper.model.Model;
import mmstutorhelper.view.MainView;

public class Main{

    public static void main(String[] args) {

        ErrorHandler.getInstanceOf().getExceptions().addListener((ListChangeListener<Exception>) c -> {
            while(c.next()){
                c.getAddedSubList().forEach(Throwable::printStackTrace);
            }
        });

        Model model=new Model();
        Thread applThread=new Thread(()->MainView.launch(MainView.class));
        applThread.start();

        MainView.waitForMainView();
        model.init();
        MainView.lastLoadedMainView.init(model);

        //model.login();
        //model.getUserIDs();
    }
}
